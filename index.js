

// console.log("Hello world")
// [SECTION] - Functions
// Functions in jS are lines/block of codes that will tell our device/application to perform a particular tasks
// Functions are mostly created to create complicated tasks to run several lines of code succession
// They are also used to prevent repeating lines of codes

//Function Declaration


// Syntax:
// function functionName () {}

// function printName() {
// 	console.log('My Name is John')
// }

// printName()

let variableFunction = function() {
	console.log('Hello')
}

variableFunction()

let functionExpression = function funcName() {
	console.log('Hello from the other side');
}

functionExpression()
// funcName() --> returns error funcName is undefined 

// [SECTION] - Function Scoping

/*
1. Local/Block Scope
2. Global Scope
3. Function Scope
*/


{
	let localVariable = "Aramando Perez"; // localvariable exclusive inside a scope
}

let globalVariable = "Mr. Worlwide"; // global variable accessible anywhere

// console.log(globalVariable)

function showNames() {
	var functionVar = "Joe";
	const functionConst = "John"
	let funcLet = "Jane"

	// console.log(functionVar)
	// console.log(functionConst)
	// console.log(funcLet)
}

showNames()


function myNewFunction(){
	let name = "Janet"

	function nestedFunction(){
		let nestedName = "John"
		console.log(name)
	}
	nestedFunction()
}
myNewFunction()


// Function and Global scope variables

let globalName = "Alexandro"

function myNewFunction2() {
	let nameInside = "Renz"

	console.log(globalName)
}

myNewFunction2()
// console.log(nameInside)

// [SECTION] - Using Alert
// alert() alows us to show samll windows at the top of our browser

// alert("Hello World!")

function showSampleAlert() {
	alert('Hello User')
}

showSampleAlert()

console.log('I will only display in the console when alert is dismissed')

// Notes on the use of alert()
	// Show on the alert() for short dialog message.
	// do not overuse alert()

// [SECTION] - Using prompt()
// prompt() allows us to show a small window and gather user input.
// usually prompt are stored in a variable

// let samplePrompt = prompt('Enter your Name.')

// console.log('Hello, ' + samplePrompt)
// console.log(typeof samplePrompt)

// let sampleNullPrompt = prompt('Do not enter anything');

// console.log(sampleNullPrompt)

function printWelcomeMessage() {
	let firstName = prompt('Enter your first name')
	let lastName = prompt('Enter your last name')
	console.log('Hello, ' + firstName + ' ' + lastName + '!')
	console.log('Welcome to my Page')
}

// printWelcomeMessage()


// [SECTION] - fUNCTION Naming Convention
// Function names hould be definitive of the task will perform. It usually contains verb.

function getCourse() {
	let courses = ['Sciencd 101', 'Math 101', 'English 101']
	console.log(courses)
}

getCourse()

// Avoid generic names to avoid confusion within your code